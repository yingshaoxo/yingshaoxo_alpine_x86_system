/* Generated automatically. */
static const char configuration_arguments[] = "/home/buildozer/aports/main/gcc/src/gcc-4.8.2/configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --build=i486-alpine-linux-musl --host=i486-alpine-linux-musl --target=i486-alpine-linux-musl --with-pkgversion='Alpine 4.8.2' --enable-checking=release --disable-fixed-point --disable-libstdcxx-pch --disable-multilib --disable-nls --disable-werror --disable-symvers --enable-__cxa_atexit --enable-esp --enable-cloog-backend --enable-languages=c,c++,objc,java,fortran,ada --with-arch=i486 --with-tune=generic --enable-cld --disable-libssp --disable-libmudflap --disable-libsanitizer --enable-shared --enable-threads --enable-tls --with-system-zlib";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "i486" }, { "arch", "i486" }, { "tune", "generic" } };
